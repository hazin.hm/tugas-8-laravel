@extends('page.layout.master')
@section('title')
Halaman Forum
@endsection
@section('content')
    <h3>Buat Account Baru</h3>
    <h4>Sign Up Forum</h4>
    <form action="/welcome" method="post">
        @csrf
        
        <label>First name :</label><br>
        <input type="text"name="First_name"><br><br>
        <label>Last name :</label><br>
        <input type="text" name="Last_name"><br><br>

        <label>Gender</label><br>
        <input type="radio" name="gender" value="male" >Male<br>
        <input type="radio" name="gender" value="female" >Female<br><br>

        <label>Nationality</label><br>
        <select name="Nationality" id="">
            <option name="nationality" value="Indonesia">Indonesia</option>
            <option name="nationality" value="Malesiya">Malesiya</option>
            <option name="nationality" value="Singapura">Singapura</option>
        </select><br><br>

        <label>Langunage Spoken</label><br>
        <input type="checkbox" name="Langunge" value="Indonesia" >Bahasa Indonesia<br>
        <input type="checkbox" name="Langunge" value="English" >English<br>
        <input type="checkbox" name="Langunge" value="Other" >Other<br><br>

        <label>Bio Data</label><br>
        <textarea name="Bio" cols="30" rows="10"></textarea><br><br>

        <input type="submit" value="Sign Up">

    </form>
@endsection
